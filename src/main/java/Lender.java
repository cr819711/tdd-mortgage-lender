import java.sql.Timestamp;
import java.util.Calendar;

final class Lender {
	private long avaliableFunds = 1000;
	private long pendingFunds = 0;
	private double interestRate = .04;

	Lender() {
	}

	public long getAvaliableFunds() {
		return avaliableFunds;
	}

	public long getPendingFunds() {
		return pendingFunds;
	}

	public void setAvaliableFunds(long avaliableFunds) {
		this.avaliableFunds = avaliableFunds;
	}

	public void addFunds(int amt) {
		this.avaliableFunds += amt;
	}

	public boolean validateLoan(LoanApplicant applicant, int months) {
		if (avaliableFunds < applicant.getRequestedAmount()) {
			return false;
		}

		if (applicant.getCreditScore() <= 620) {
			return false;
		}

		if ((applicant.getMonthlyDebtLoad()
				+ estimateMonthlyPayments(applicant, months)) > (applicant.getMonthlyGrossIncome() * 0.36)) {
			return false;
		}

		if (applicant.getDownPayment() < (0.25 * applicant.getRequestedAmount())) {
			return false;
		}
		return true;
	}

	public double estimateMonthlyPayments(LoanApplicant applicant, int months) {
		int numberOfPayments = months;
		long principal = applicant.getRequestedAmount() - applicant.getDownPayment();
		// System.out.println("Principal : " + principal);

		double monthlyInterestRate = interestRate / 12;
		// System.out.println("Monthly Interest Rate : " + monthlyInterestRate);

		double onePlusInterestRate = monthlyInterestRate + 1.00000;
		// System.out.println("One Plus : " + onePlusInterestRate);

		double numerator = monthlyInterestRate * Math.pow(onePlusInterestRate, numberOfPayments);
		// System.out.println("Numerator : " + numerator);

		double denominator = Math.pow(onePlusInterestRate, numberOfPayments) - 1;
		// System.out.println("Denominator : " + denominator);

		double monthlyPayment = principal * (numerator / denominator);

		// System.out.println("Monthly payment : " + monthlyPayment);
		return monthlyPayment;
	}

	public ApplicationStatus offerLoan(LoanApplicant applicant, int months) {
		if (validateLoan(applicant, months)) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Timestamp(System.currentTimeMillis()));
			cal.add(Calendar.DAY_OF_WEEK, 3);
			applicant.setExpirationDate(new Timestamp(cal.getTime().getTime()));
			return ApplicationStatus.APPROVED;
		}
		return ApplicationStatus.DENIED;
	}

	public void sendMoney(LoanApplicant applicant) {
		if (applicant.getAcceptance() == ApplicationStatus.ACCEPTED) {
			avaliableFunds = avaliableFunds - applicant.getRequestedAmount();
			pendingFunds += applicant.getRequestedAmount();
		}
	}

	public void reclaimFunds(LoanApplicant applicant) {
		if (applicant.getExpirationDate().before(new Timestamp(System.currentTimeMillis()))) {
			avaliableFunds += applicant.getRequestedAmount();
			pendingFunds -= applicant.getRequestedAmount();
		}
	}
}
