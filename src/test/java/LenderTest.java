import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;

class LenderTest {

	@Test
	public void checkInitialFunds() {
		Lender lender = new Lender();

		assertEquals(lender.getAvaliableFunds(), 1000);
	}

	@Test
	public void addFunds() {
		Lender lender = new Lender();
		lender.addFunds(1000);
		assertEquals(lender.getAvaliableFunds(), 2000);
	}

	@Test
	public void denyApplicants() {
		Lender lender = new Lender();
		LoanApplicant applicant = new LoanApplicant(50, 1001, 1000, 1000, 10);
		assertFalse(lender.validateLoan(applicant, 360));
	}

	@Test
	public void estimateMonthlyPayments() {

		Lender lender = new Lender();
		LoanApplicant applicant = new LoanApplicant(0, 250000, 1000, 1000, 1000);
		assertEquals(lender.estimateMonthlyPayments(applicant, 360), 1193.5382386636345);
	}

	@Test
	public void validateApplicant_Good() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		assertTrue(lender.validateLoan(applicant, 360));
	}

	@Test
	public void validateApplicant_Bad() {
		Lender lender = new Lender();
		LoanApplicant applicant = new LoanApplicant(0, 250000, 1000, 1000, 1000);
		assertFalse(lender.validateLoan(applicant, 360));
	}

	@Test
	public void offerLoan() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		assertEquals(lender.offerLoan(applicant, 360), ApplicationStatus.APPROVED);
	}

	@Test
	public void acceptLoan() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		applicant.acceptLoan(true);
		assertEquals(applicant.getAcceptance(), ApplicationStatus.ACCEPTED);
	}

	@Test
	public void sendMoney() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		applicant.acceptLoan(true);
		lender.sendMoney(applicant);
		assertEquals(751000, lender.getAvaliableFunds());
	}

	@Test
	public void setExpiredate() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		lender.offerLoan(applicant, 360);
		applicant.acceptLoan(true);
		assertTrue(applicant.getExpirationDate().after(new Timestamp(System.currentTimeMillis())));
	}

	@Test
	public void testPending() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		applicant.acceptLoan(true);
		lender.sendMoney(applicant);
		assertEquals(250000, lender.getPendingFunds());
	}

	@Test
	public void reclaimFunds() {
		Lender lender = new Lender();
		lender.addFunds(1000000);
		LoanApplicant applicant = new LoanApplicant(90000, 250000, 50000, 1000, 1000);
		lender.offerLoan(applicant, 360);
		applicant.acceptLoan(true);
		applicant.setExpirationDate(new Timestamp(System.currentTimeMillis() - 100000));
		lender.reclaimFunds(applicant);
	}
}
